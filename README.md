# keuler

This is just a fluff project created for me to learn Kotlin while solving relatively easy problems 
(in this case, Project Euler problems) in an enterprise-y way.

The idea is that if the problems are easy to solve, you write more code per unit time, and that might
assist in learning a language's syntax and utilities faster.
