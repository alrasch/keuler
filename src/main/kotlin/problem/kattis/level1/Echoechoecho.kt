package problem.kattis.level1

fun main() {
    val inp = readln()
    println((0 until 3).joinToString(" ") { inp })
}