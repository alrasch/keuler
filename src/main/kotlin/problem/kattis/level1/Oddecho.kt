package problem.kattis.level1

fun main() {
    val n = readln().toInt()

    val words = mutableListOf<String>()
    for (i in 0 until n) {
        words.add(readln())
    }
    words.forEachIndexed { index, value ->
        if (index % 2 == 0) {
            println(value)
        }
    }
}