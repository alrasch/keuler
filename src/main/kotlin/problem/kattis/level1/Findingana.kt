package problem.kattis.level1

fun main() {
    val s = readln()
    println(s.substring(s.indexOf("a")))
}