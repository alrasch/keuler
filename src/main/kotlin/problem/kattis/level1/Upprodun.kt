package problem.kattis.level1

fun main() {
    val rooms = readln().toInt()
    val teams = readln().toInt()

    val fixed = teams / rooms
    val remainder = teams % rooms

    val roomAssignments = mutableListOf<String>()
    for (a in 0 until rooms) {
        roomAssignments.add("*".repeat(fixed))
    }

    for (a in 0 until remainder) {
        roomAssignments[a] = "${roomAssignments[a]}*"
    }

    roomAssignments.forEach { println(it) }
}