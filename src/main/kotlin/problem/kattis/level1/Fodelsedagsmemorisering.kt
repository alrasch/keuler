package problem.kattis.level1


fun main() {
    val n = readln().toInt()

    val bdays = mutableListOf<Tuple>()

    for (i in 0 until n) {
        val (name, like, bday) = readln().split(" ")
        bdays.add(Tuple(name, like.toInt(), bday))
    }

    val toRemove = mutableListOf<Tuple>()

    for (a in bdays) {
        for (b in bdays) {
            if (a.bday == b.bday && a.name != b.name) {
                if (a.like > b.like) {
                    toRemove.add(b)
                } else {
                    toRemove.add(a)
                }
            }
        }
    }

    bdays.removeAll(toRemove)
    println(bdays.size)
    bdays.sortBy{ it.name }
    bdays.forEach { println(it.name) }
}

data class Tuple(val name: String, val like: Int, val bday: String)
