package problem.kattis.level1

fun main() {
    println(readln().split(" ").map{ it.toInt() }.reduce{ sum, i -> sum + i })
}