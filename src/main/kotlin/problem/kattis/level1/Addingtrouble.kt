package problem.kattis.level1

fun main() {
    val i = readln().split(" ").map{ it.toInt() }
    println(if (i[0] + i[1] == i[2]) "correct!" else "wrong!")
}