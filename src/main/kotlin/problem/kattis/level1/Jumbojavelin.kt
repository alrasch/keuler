package problem.kattis.level1

fun main() {
    val n = readln().toInt()

    val lens = mutableListOf<Int>()
    for (i in 0 until n) {
        lens.add(readln().toInt())
    }

    println(lens.reduce{ sum, i -> i + sum } - (lens.size - 1))
}