package problem.kattis.level1

fun main() {
    val inp = readln().split(" ").map{ it.toInt() }
    println(if (inp[0] > inp[1]) 1 else 0)
}