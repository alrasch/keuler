package problem.kattis.level1

fun main() {
    val s = readln()
    val n = readln().toInt()

    println(s.repeat(n))
}