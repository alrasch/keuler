package problem.kattis.level1

fun main() {
    val n = readln().toInt()
    println((n % 10)*10 + (n - (n % 10)) / 10)
}