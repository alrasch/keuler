package problem.kattis.level1

fun main() {
    val p = readln().toInt()

    if ((p - (p % 10000)) / 10000 == 555) {
        println(1)
    } else {
        println(0)
    }
}