package problem.kattis.level1

fun main() {
    val n = readln().toInt()
    println(readln().split(" ").map{ it.toInt() }.reduce{ sum, i -> sum + i })
}