package problem.kattis.level1

fun main() {
    val p = readln().toInt()
    val ns = mutableListOf<Int>()
    (0 until p).map{
        ns.add(readln().toInt())
    }

    for (n in ns) {
        println(
            when (n) {
                1 -> { "1" }
                2 -> { "2" }
                3 -> { "6" }
                4 -> { "4" }
                else -> { "0" }
            }
        )
    }
}
