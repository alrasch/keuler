package problem.kattis.level1

fun main() {
    val a = readln().toFloat()/100
    val b = 1-a
    val aOut = 1/a
    val bOut = 1/b
    println(aOut)
    println(bOut)
}