package problem.kattis.level1

fun main() {
    val n = readln().toInt()
    val balls = mutableListOf<String>()
    (0 until n).map{ balls.add(readln()) }

    val sorted = mutableMapOf<String, Int>()
    balls.forEach {
        val a = it.split(" ")
        try {
            sorted[a[0]] = a[1].toInt()
        } catch (e: NumberFormatException) {
            sorted[a[1]] = a[0].toInt()/2
        }
    }

    sorted.toList().sortedBy {(_, value) -> value}.forEach{ (color, _) -> println(color) }
}