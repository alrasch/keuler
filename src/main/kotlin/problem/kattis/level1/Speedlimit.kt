package problem.kattis.level1

fun main() {
    val all = mutableListOf<Int>()
    while (true) {
        val n = readln().toInt()
        if (n == -1) {
            break
        }

        var sum = 0
        var travelled = 0
        (0 until n).forEach{
            val travel = readln().split(" ").map{ it.toInt() }
            val speed = travel[0]
            val distance = travel[1] - travelled
            sum += (speed * distance)
            travelled = travel[1]
        }
        all.add(sum)
    }
    all.forEach{println("$it miles")}
}