package problem.kattis.level1

fun main() {
    println(readln().toInt().toString(2).reversed().toInt(2))
}