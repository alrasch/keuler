package problem.kattis.level1

fun main() {
    val strings = listOf("a", "b", "c", "d")
    println(strings.reduceIndexed { index, acc, string -> acc + string + index })
}