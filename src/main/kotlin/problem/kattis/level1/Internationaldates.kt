package problem.kattis.level1

fun main() {
    val a = readln()

    val (d, m, _) = a.split("/")

    if (d.toInt() > 12) {
        println("EU")
    } else if (m.toInt() > 12) {
        println("US")
    } else {
        println("either")
    }
}