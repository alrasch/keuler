package problem.kattis.trivial

fun main() {
    val a = readln().toInt()
    println(a * 0.09144)
}