package problem.kattis.trivial

fun main() {
    println(if (readln().toInt() % 2 == 0) "second" else "first")
}
