package problem.kattis.trivial

fun main() {
    val n = readln().toInt()
    val k = readln().toInt()

    println(2022 + n/k)
}