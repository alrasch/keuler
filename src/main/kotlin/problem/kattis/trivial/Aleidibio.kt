package problem.kattis.trivial

fun main() {
    val a = readln().toInt()
    val b = readln().toInt()
    val c = readln().toInt()

    println(c-a-b)
}
