package problem.kattis.trivial

fun main() {
    val a = readln()
    val b = a.count { it == 'b' }
    val k = a.count { it == 'k' }

    val res = if (b > k) {
        "boba"
    } else if (k > b) {
        "kiki"
    } else if (b >= 1) {
        "boki"
    } else {
        "none"
    }

    println(res)
}