package problem.kattis.trivial

fun main() {
    val n = readln()
    val t = readln().split(" ")

    var sum = 0
    for (i in t) {
        if (i.toInt() < 0) {
            sum++
        }
    }
    println(sum)
}