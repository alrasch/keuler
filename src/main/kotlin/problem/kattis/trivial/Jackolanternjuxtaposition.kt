package problem.kattis.trivial

fun main() {
    println(readln().split(" ").map{ it.toInt() }.reduce{ prod, i -> i * prod })
}