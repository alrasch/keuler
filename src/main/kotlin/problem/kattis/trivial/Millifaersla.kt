package problem.kattis.trivial

fun main() {
    val m = readln().toInt()
    val f = readln().toInt()
    val d = readln().toInt()

    if (m == minOf(m, f, d)) println("Monnei")
    if (f == minOf(m, f, d)) println("Fjee")
    if (d == minOf(m, f, d)) println("Dolladollabilljoll")
}