package problem.kattis.trivial

fun main() {
    val h = readln()

    val num = h.count { it == 'e'}

    var s = "h"
    for (i in 0 until num) {
        s += "ee"
    }
    s += "y"
    println(s)
}