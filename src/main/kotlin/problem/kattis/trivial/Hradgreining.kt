package problem.kattis.trivial

fun main() {
    println(if (readln().contains("COV")) "Veikur!" else "Ekki veikur!")
}