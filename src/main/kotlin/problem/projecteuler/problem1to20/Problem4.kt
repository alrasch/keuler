package problem.projecteuler.problem1to20

import util.string.Palindrome

class Problem4 {
    fun solve(): Int {
        val palindromeUtil = Palindrome()
        var highest = 0

        (999 downTo 900).forEach { i ->
            (999 downTo 900).forEach { j ->
                if (palindromeUtil.isPalindrome((i * j).toString()) && i*j > highest) {
                    highest = i*j
                }
            }
        }
        return highest
    }
}
