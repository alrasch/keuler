package problem.projecteuler.problem1to20

class Problem7 {
    fun solve(): Int {
        val primes = mutableListOf(2, 3)
        var numToCheck = 5
        outer@ while (primes.size < 10001) {
            for (prime in primes) {
                if (numToCheck % prime == 0) {
                    numToCheck += 2
                    continue@outer
                }
            }
            primes.add(numToCheck)
            numToCheck += 2
        }
        return primes.last()
    }
}
