package problem.projecteuler.problem1to20

import util.prime.Prime

class Problem10 {
    fun solve(): Long {
        val p = Prime()
        val a = p.getPrimesUpTo(2_000_000)

        var sum = 0L
        a.forEach {
            sum += it
        }
        return sum
    }
}
