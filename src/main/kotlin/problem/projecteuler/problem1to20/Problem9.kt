package problem.projecteuler.problem1to20

import util.power.Power
import kotlin.math.sqrt

class Problem9 {
    fun solve(): Int {
        val p = Power()
        for (i in 1..998) {
            for (j in i..998) {
                val lhs = i * i + j * j
                if (!p.isPerfectSquare(lhs)) {
                    continue
                }
                val k = sqrt(lhs.toDouble()).toInt()

                if (i + j + k == 1000) {
                    return i*j*k
                }
            }
        }
        return -1
    }
}
