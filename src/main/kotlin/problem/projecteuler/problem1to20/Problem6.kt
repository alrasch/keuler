package problem.projecteuler.problem1to20

import util.CollectionUtil
import kotlin.math.abs

class Problem6 {
    fun solve(): Int {
        val collectionUtil = CollectionUtil()
        val range = (1..100).toList()
        val sumOfSquares = collectionUtil.sumOfSquares(range)
        val squareOfSum = collectionUtil.squareOfSum(range)

        return abs(sumOfSquares - squareOfSum)
    }
}
