package problem.projecteuler.problem1to20

import util.divisibility.Divisibility
import util.prime.Prime

class Problem12 {
    fun solve(): Int {
        val div = Divisibility()
        val prime = Prime()
        val primes = prime.getPrimesUpTo(1_000_000)

        var tri = 1
        var adder = 2
        var highest = 0
        while (true) {
            val b = div.numberOfDivisors(tri, primes)
            highest = maxOf(highest, b)

            if (highest >= 500) {
                return tri
            }
            tri += adder
            adder += 1
        }
    }
}
