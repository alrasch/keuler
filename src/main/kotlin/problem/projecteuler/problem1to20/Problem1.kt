package problem.projecteuler.problem1to20

import io.quarkus.runtime.annotations.QuarkusMain
import util.CollectionUtil

@QuarkusMain
class Problem1 {
    fun solve(): Int {
        val multiples = mutableListOf<Int>()
        for (i in 3..999) {
            if (i % 3 == 0 || i % 5 == 0) {
                multiples.add(i)
            }
        }

        val cUtil = CollectionUtil()
        val sum = cUtil.sumList(multiples)

        return sum
    }
}
