package problem.projecteuler.problem1to20

import util.prime.Prime
import kotlin.math.floor
import kotlin.math.sqrt

class Problem3 {
    fun solve(): Int {
        val primeUtil = Prime()
        val target: Long = 600851475143
        var solution = -1

        var highestCandidate = (floor(sqrt(target.toDouble()))+1).toInt()

        if (highestCandidate % 2 == 0) {
            highestCandidate -= 1
        }

        for (i in highestCandidate downTo 3 step 2) {
            if ((target % i).toInt() == 0 && primeUtil.isPrime(i)) {
                solution = i
                break
            }
        }

        return solution
    }
}
