package problem.projecteuler.problem1to20

class Problem14 {
    fun solve(): Long {
        val stepMap = mutableMapOf<Long, Int>()

        for (i in 1L..1_000_000L) {
            var current = i
            var steps = 1
            while (true) {
                if (current % 2L == 0L) {
                    current /= 2
                } else {
                    current = 3*current + 1
                }

                if (stepMap.containsKey(current)) {
                    steps += stepMap[current]!!
                    stepMap[i] = steps
                    break
                } else {
                    steps++
                }

                if (current == 1L) {
                    stepMap[i] = steps
                    break
                }
            }
        }
        var highestStepCount = 0
        var highestStart = 0L
        for (i in stepMap.keys) {
            if (stepMap[i]!! > highestStepCount) {
                highestStepCount = stepMap[i]!!
                highestStart = i
            }
        }

        return highestStart
    }
}
