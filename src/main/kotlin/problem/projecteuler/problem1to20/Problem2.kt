package problem.projecteuler.problem1to20

class Problem2 {
    fun solve(): Int {
        val fib = mutableListOf(1, 2)
        while (fib[fib.size - 1] + fib[fib.size - 2] < 4000000) {
            fib.add(fib[fib.size - 1] + fib[fib.size - 2])
        }

        var sum = 0
        for (i in fib) {
            if (i % 2 == 0) {
                sum += i
            }
        }
        return sum
    }
}