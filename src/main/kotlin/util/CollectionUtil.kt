package util

class CollectionUtil() {

    inline fun <reified T : Number> sumList(myList: List<T>): T {
        var sum: T = when (T::class) {
            Int::class -> 0 as T
            Long::class -> 0L as T
            Float::class -> 0f as T
            Double::class -> 0.0 as T
            else -> throw IllegalArgumentException("Tried to sum an unsupported number type")
        }

        myList.forEach {
            sum = addNumbers(sum, it)
        }

        return sum
    }

    inline fun <reified T : Number> addNumbers(a: T, b: T): T {
        return when (T::class) {
            Int::class -> (a.toInt() + b.toInt()) as T
            Long::class -> (a.toLong() + b.toLong()) as T
            Float::class -> (a.toFloat() + b.toFloat()) as T
            Double::class -> (a.toDouble() + b.toDouble()) as T
            else -> throw IllegalArgumentException("Tried to sum an unsupported number type")
        }
    }

    fun sumOfSquares(numbers: List<Int>): Int {
        var sum = 0
        numbers.forEach {
            sum += (it*it)
        }
        return sum
    }

    fun squareOfSum(numbers: List<Int>): Int {
        var sum = 0
        numbers.forEach { sum += it }
        return sum * sum
    }
}
