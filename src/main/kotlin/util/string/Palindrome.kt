package util.string

import kotlin.math.ceil

class Palindrome {
    fun isPalindrome(x: String): Boolean {
        val length = x.length
        for (i in (0 until ceil(length.toDouble() / 2).toInt())) {
            if (x[i] != x[length-1-i]) {
                return false
            }
        }
        return true
    }
}