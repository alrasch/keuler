package util.power

import kotlin.math.ceil
import kotlin.math.sqrt

class Power {
    fun isPerfectSquare(n: Int): Boolean {
        return sqrt(n.toDouble()) == ceil(sqrt(n.toDouble()))
    }
}
