package util.prime

import kotlin.math.floor
import kotlin.math.sqrt

class Prime {
    fun isPrime(n: Int): Boolean {
        if (n < 1) {
            throw IllegalArgumentException("Numbers below 1 cannot be prime")
        }
        if (n == 1) {
            return false
        }

        if (n == 2 || n == 3) {
            return true
        }

        for (i in 2..floor(sqrt(n.toDouble())).toInt()+1) {
            if (n % i == 0) {
                return false
            }
        }

        return true
    }

    /**
     * Get list of all primes smaller than n.
     *
     * Uses Sieve of Erastothenes.
     */
    fun getPrimesUpTo(n: Int): List<Int> {
        val primes = BooleanArray(n+1) { true }
        val sqrtN = sqrt(n.toDouble())
        outer@ for (index in primes.indices) {
            if (index == 0 || index == 1) {
                primes[index] = false
            }
            if (primes[index]) {
                for (i in index * index .. n step index) {
                    if (index >= sqrtN) {
                        continue@outer
                    }
                    primes[i] = false
                }
            }
        }

        return primes.withIndex()
            .filter { it.value }
            .map { it.index }
    }
}
