package util.divisibility

class Divisibility {
    fun numberOfDivisors(n: Int, primes: List<Int> = listOf()): Int {
        var nn = n
        val primeFactors = mutableListOf<Int>()
        var index = 0
        while (nn > 1) {
            val p = primes[index]
            if (nn % p == 0) {
                primeFactors.add(p)
                nn /= p
                index = 0
            } else {
                index++
            }
        }

        val primeMap = mutableMapOf<Int, Int>()
        for (i in primeFactors.indices) {
            val factor = primeFactors[i]
            if (primeMap[factor] == null) {
                primeMap[factor] = 1
            } else {
                primeMap[factor] = primeMap[factor]!!+1
            }
        }

        var numberOfDivisors = 1
        primeMap.values.forEach{
            numberOfDivisors *= (it + 1)
        }

        return numberOfDivisors
    }
}