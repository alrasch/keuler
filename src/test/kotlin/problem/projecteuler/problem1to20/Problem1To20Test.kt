package projecteuler.problem1to20

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.assertEquals
import problem.projecteuler.problem1to20.*

class Problem1To20Test {

    @Test
    fun `Problem1 solves correctly`() {
        val actual = Problem1().solve()
        val expectation = 233168
        assertEquals(expectation, actual)
    }

    @Test
    fun `Problem2 solves correctly`() {
        val actual = Problem2().solve()
        val expectation = 4613732
        assertEquals(expectation, actual)
    }

    @Test
    fun `Problem3 solves correctly`() {
        val actual = Problem3().solve()
        val expectation = 6857
        assertEquals(expectation, actual)
    }

    @Test
    fun `Problem4 solves correctly`() {
        val actual = Problem4().solve()
        val expectation = 906609
        assertEquals(expectation, actual)
    }

    @Test
    fun `Problem5 solves correctly`() {
        val actual = Problem5().solve()
        val expectation = 232792560
        assertEquals(expectation, actual)
    }

    @Test
    fun `Problem6 solves correctly`() {
        val actual = Problem6().solve()
        val expectation = 25164150
        assertEquals(expectation, actual)
    }

    @Test
    fun `Problem7 solves correctly`() {
        val actual = Problem7().solve()
        val expectation = 104743
        assertEquals(expectation, actual)
    }

    @Test
    fun `Problem8 solves correctly`() {
        val actual = Problem8().solve()
        val expectation = 23514624000L
        assertEquals(expectation, actual)
    }

    @Test
    fun `Problem9 solves correctly`() {
        val actual = Problem9().solve()
        val expectation = 31875000
        assertEquals(expectation, actual)
    }

    @Test
    fun `Problem10 solves correctly`() {
        val actual = Problem10().solve()
        val expectation = 142913828922L
        assertEquals(expectation, actual)
    }

    @Test
    fun `Problem11 solves correctly`() {
        val actual = Problem11().solve()
        val expectation = 70600674
        assertEquals(expectation, actual)
    }

    @Test
    fun `Problem12 solves correctly`() {
        val actual = Problem12().solve()
        val expectation = 76576500
        assertEquals(expectation, actual)
    }

    @Test
    fun `Problem13 solves correctly`() {
        val actual = Problem13().solve()
        val expectation = "5537376230"
        assertEquals(expectation, actual)
    }

    @Test
    fun `Problem14 solves correctly`() {
        val actual = Problem14().solve()
        val expectation = 837799L
        assertEquals(expectation, actual)
    }
}
