package unit.util

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import util.CollectionUtil

class CollectionUtilTest {
    private val collectionUtil = CollectionUtil()

    @Test
    fun `sumList should correctly sum a list of Integers`() {
        val intList = listOf(1, 2, 3, 4, 5)
        val expectedSum = 15
        val actualSum = collectionUtil.sumList(intList)
        assertEquals(expectedSum, actualSum)
    }

    @Test
    fun `sumList should correctly sum a list of Longs`() {
        val longList = listOf(1L, 2L, 3L, 4L, 5L)
        val expectedSum = 15L
        val actualSum = collectionUtil.sumList(longList)
        assertEquals(expectedSum, actualSum)
    }

    @Test
    fun `sumList should correctly sum a list of Floats`() {
        val floatList = listOf(1f, 2f, 3f, 4f, 5f)
        val expectedSum = 15f
        val actualSum = collectionUtil.sumList(floatList)
        assertEquals(expectedSum, actualSum)
    }

    @Test
    fun `sumList should correctly sum a list of Doubles`() {
        val doubleList = listOf(1.0, 2.0, 3.0, 4.0, 5.0)
        val expectedSum = 15.0
        val actualSum = collectionUtil.sumList(doubleList)
        assertEquals(expectedSum, actualSum)
    }

    @Test
    fun `sumList should throw IllegalArgumentException for unsupported number types`() {
        val unsupportedList = listOf<Number>(1.toByte(), 2.toByte(), 3.toByte())
        assertThrows(IllegalArgumentException::class.java) {
            collectionUtil.sumList(unsupportedList)
        }
    }
}
