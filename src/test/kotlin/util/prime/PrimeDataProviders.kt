package util.prime

import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.Arguments.arguments
import java.util.stream.Stream

class PrimeDataProviders {
    companion object {
        @JvmStatic
        fun lowPrimes(): Stream<Int> {
            return Stream.of(2, 3, 5, 7, 11)
        }

        @JvmStatic
        fun highPrimes(): Stream<Int> {
            return Stream.of(2143, 50549, 777743)
        }

        @JvmStatic
        fun composites(): Stream<Int> {
            return Stream.of(
                2 * 2143,
                3 * 50549,
                5 * 7777743,
                7 * 11 * 13 * 23,
                2143 * 50549
            )
        }

        @JvmStatic
        fun invalidInputs(): Stream<Int> {
            return Stream.of(
                0,
                -1,
                -11,
            )
        }

        @JvmStatic
        fun consecutivePrimes(): Stream<Arguments> {
            return Stream.of(
                arguments(19, listOf(2, 3, 5, 7, 11, 13, 17, 19)),
                arguments(20, listOf(2, 3, 5, 7, 11, 13, 17, 19))
            )
        }
    }
}
