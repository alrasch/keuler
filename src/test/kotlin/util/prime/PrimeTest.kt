package util.prime

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

class PrimeTest {
    private val primeUtil = Prime()

    @ParameterizedTest
    @MethodSource("util.prime.PrimeDataProviders#lowPrimes")
    fun `correctly classifies low primes`(prime: Int) {
        assertTrue(primeUtil.isPrime(prime))
    }

    @ParameterizedTest
    @MethodSource("util.prime.PrimeDataProviders#highPrimes")
    fun `correctly classifies larger primes`(prime: Int) {
        assertTrue(primeUtil.isPrime(prime))
    }

    @ParameterizedTest
    @MethodSource("util.prime.PrimeDataProviders#composites")
    fun `correctly classifies composite number`(composite: Int) {
        assertFalse(primeUtil.isPrime(composite))
    }

    @ParameterizedTest
    @MethodSource("util.prime.PrimeDataProviders#invalidInputs")
    fun `correctly throws on invalid input`(invalidInput: Int) {
        assertThrows(IllegalArgumentException::class.java) {
            primeUtil.isPrime(invalidInput)
        }
    }

    @ParameterizedTest
    @MethodSource("util.prime.PrimeDataProviders#consecutivePrimes")
    fun `correctly returns primes up to input`(input: Int, expectation: List<Int>) {
        assertEquals(expectation, primeUtil.getPrimesUpTo(input))
    }
}