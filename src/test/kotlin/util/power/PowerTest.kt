package util.power

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class PowerTest {
    val sut = Power()

    @Test
    fun `it correctly identifies perfect squares`() {
        val squares = listOf(0, 1, 4, 9, 16, 25, 36, 49, 100, 400, 10_000, 1_000_000)
        val nonSquares = listOf(2, 3, 5, 6, 7, 8, 10, 123, 321, 10_001, 1_000_001)

        squares.forEach {
            assertTrue(sut.isPerfectSquare(it))
        }

        nonSquares.forEach {
            assertFalse(sut.isPerfectSquare(it))
        }
    }
}
