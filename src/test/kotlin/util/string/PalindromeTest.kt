package util.string

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class PalindromeTest {
    private val sut = Palindrome()

    @Test
    fun `correctly classifies palindromes`() {
        val palindromes = listOf("aba", "9009", "123454321", "COBOC", "agnes i senga", "", "a", "bb")

        palindromes.forEach {
            assertTrue(sut.isPalindrome(it))
        }
    }

    @Test
    fun `correctly classifies non-palindromes`() {
        val nonPalindromes = listOf("123453321", "cOBOL", "9001")

        nonPalindromes.forEach {
            assertFalse(sut.isPalindrome(it))
        }
    }
}
